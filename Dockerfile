FROM alpine
RUN exec 2>&1 \
    && set -ex \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing --virtual .locales-rundeps \
        musl-locales \
    && apk add --no-cache --virtual .postgresql-rundeps \
        openssh-client \
        openssh-server \
        pgpool \
        postgresql \
        postgresql-contrib \
        repmgr \
        repmgr-daemon \
        rsync \
        runit \
        shadow \
        tzdata \
    && echo done
ADD bin /usr/local/bin
ADD service /etc/service
CMD [ "runsvdir", "/etc/service" ]
ENTRYPOINT [ "docker_entrypoint.sh" ]
ENV HOME=/var/lib/postgresql
ENV GROUP=postgres \
    PGDATA="${HOME}/pg_data" \
    USER=postgres
VOLUME "${HOME}"
WORKDIR "${HOME}"
RUN exec 2>&1 \
    && set -ex \
    && sed -i -e 's|#PasswordAuthentication yes|PasswordAuthentication no|g' /etc/ssh/sshd_config \
    && sed -i -e 's|#   StrictHostKeyChecking ask|   StrictHostKeyChecking no|g' /etc/ssh/ssh_config \
    && echo "   UserKnownHostsFile=/dev/null" >>/etc/ssh/ssh_config \
    && sed -i -e 's|postgres:!:|postgres::|g' /etc/shadow \
    && chmod -R 0755 /etc/service /usr/local/bin \
    && rm -rf /usr/src /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man \
    && echo done
