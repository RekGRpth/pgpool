#!/bin/sh -ex

#docker build --tag rekgrpth/pgpool .
#docker push rekgrpth/pgpool
#docker pull rekgrpth/pgpool
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create pgpool
docker stop pgpool || echo $?
docker rm pgpool || echo $?
docker run \
    --detach \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname pgpool \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=pgpool,destination=/var/lib/postgresql \
    --name pgpool \
    --network name=docker \
    --restart always \
    rekgrpth/pgpool
