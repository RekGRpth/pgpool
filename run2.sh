#!/bin/sh -ex

#docker build --tag rekgrpth/pgpool .
#docker push rekgrpth/pgpool
#docker pull rekgrpth/pgpool
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create pgpool2
docker volume create gfs
docker stop pgpool2 || echo $?
docker rm pgpool2 || echo $?
docker run \
    --detach \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname pgpool2 \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=gfs,destination=/var/lib/postgresql/gfs \
    --mount type=volume,source=pgpool2,destination=/var/lib/postgresql \
    --name pgpool2 \
    --network name=docker \
    --publish target=5432,published=5434,mode=host \
    --publish target=9999,published=5435,mode=host \
    --restart always \
    rekgrpth/pgpool
